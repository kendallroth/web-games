module.exports = {
  collectCoverage: true,
  // NOTE: Enable to collect coverage from all appropriate files, not just tested ones!
  // collectCoverageFrom: [
  //   "src/**/*.{ts,vue}",
  //   "!src/{plugins,types}/**",
  // ],
  preset: "@vue/cli-plugin-unit-jest/presets/typescript-and-babel",
  moduleNameMapper: {
    "^@components(.*)$": "<rootDir>/src/components$1",
    "^@typings(.*)$": "<rootDir>/src/types$1",
    "^@utilities(.*)$": "<rootDir>/src/utilities$1",
    "^@views(.*)$": "<rootDir>/src/views$1",
  },
  setupFilesAfterEnv: ["<rootDir>src/setupTests.ts"],
  testMatch: ["**/*.test.ts"],
  // NOTE: Need to transpile/transform some node_module packages during tests
  transformIgnorePatterns: ["/node_modules/(?!(vuetify)/)"],
};
