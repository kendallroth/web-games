# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.4.2] - 2021-01-22
### Changed
- Improved initial minesweeper mine placement

## [2.4.1] - 2021-01-22
### Fixed
- Snake game did not reset between games

## [2.4.0] - 2021-01-22
### Added
- Snake game

### Fixed
- Standardized grid generation to move from top left to bottom right
  - Required changing all 2D array references to `[y][x]` format

## [2.3.0] - 2021-01-21
### Added
- Ability to save and load game configurations

## [2.2.0] - 2021-01-19
### Added
- Additional basic tests
- Support for "questioned" Minesweeper tiles 

### Changed
- Implemented restart game logic in both games (previously regenerated)
- Refactored types directory structure

## [2.1.0] - 2021-01-18
### Added
- Minesweeper game

## [2.0.0] - 2021-01-17
### Changed
- Overhauled project structure to support multiple games and replaced URL
- Updated flood select logic to allow for more complex interactions

## [1.1.1] - 2021-01-16
### Fixed
- Moved `_redirects` file to `public/` directory

## [1.1.0] - 2021-01-16
### Changed
- Renamed project to "Flood It" and updated URL

## [1.0.0] - 2021-01-15
### Added
- Initial VueJS website and Flood It game
- GitLab CI pipelines for testing/linting
- Netlify deployment process
