# [Web Games](https://games.kendallroth.ca)

VueJS implementations of several popular games.

- [Flood It](https://games.kendallroth.ca/flood-it/)
- [Minesweeper](https://games.kendallroth.ca/minesweeper/)

<div align="center">
  <a href="https://games.kendallroth.ca">
    <img src="src/assets/images/web_games.png" width="500" />
  </a>
</div>

> Mobile responsiveness is not yet implemented; still determining best approach.

## Development

- Built with [VueJS](https://vuejs.org/)
- Components from [Vuetify](https://vuejs.org/)
- Tested with [Jest](https://jestjs.io/)
- Tracked with [GitLab](https://gitlab.com/)
- Deployed with [Netlify](https://www.netlify.com/)

### Scripts

```bash
# Install dependencies
npm install

# Serve development version with hot-reloading
npm run serve

# Compile and minify for production
npm run build

# Run unit tests
npm run test:unit

# Lint and fix code
npm run lint
```

## Notes

- 2D array coordinates are referenced as `[y][x]`, so that the grid system can more from top-left to bottom-right (with `y` holding rows).

## Tests

Tests in this project are primarily reserved for utility functions (not components).

> **NOTE:** Test coverage currently only includes tested files (many are untested)!

> Testing `shallowMount` Vue components with TypeScript requires declaring the wrapper component type as such:
>
> `let wrapper: Wrapper<COMPONENT & { [key: string]: any }>`
>
> _Taken from: https://github.com/vuejs/vue-test-utils/issues/255#issuecomment-628628682_

## Miscellaneous

- Icon generated with [Expo Icon Builder](https://buildicon.netlify.app/)
- PWA assets generated with [`vue-pwa-asset-generator`](https://www.npmjs.com/package/vue-pwa-asset-generator)
