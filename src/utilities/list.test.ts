// Utilities
import { randomIndex, random2DItem, randomItem } from "@utilities/list";

const list = ["red", "green", "blue", "yellow", "purple", "orange"];
const list2D = [
  ["red", "green", "blue"],
  ["one", "two", "three"],
];

describe("Randomly selects an index", () => {
  it("Stays within bounds", () => {
    const exclusiveIndex = 5;

    // Calculate many random indexes to ensure a wide spread of numbers/randomness
    const indexes: number[] = [];
    for (let i = 0; i < 100; i++) {
      indexes.push(randomIndex(exclusiveIndex));
    }

    const maxIndex = Math.max(...indexes);
    const minIndex = Math.min(...indexes);

    expect(minIndex).toBeGreaterThanOrEqual(0);
    expect(maxIndex).toBeLessThan(exclusiveIndex);
  });
});

describe("Randomly selects a value from a list", () => {
  it("Selects from all items", () => {
    // Select many random items to ensure a wide spread of numbers/randomness
    const selectedItems: Record<string, boolean> = {};
    for (let i = 0; i < 100; i++) {
      const item = randomItem(list);
      selectedItems[item] = true;
    }

    const firstItem = list[0];
    const lastItem = list[list.length - 1];

    expect(selectedItems[firstItem]).toBe(true);
    expect(selectedItems[lastItem]).toBe(true);
  });

  // NOTE: Disabled functionality as this is unlikely
  /*it("Handles empty list", () => {
    const emptyList: number[] = [];

    const item = randomItem(emptyList);

    expect(item).toBe(null);
  });*/
});

describe("Randomly selects a value from a 2D list", () => {
  it("Selects from all items", () => {
    // Select many random items to ensure a wide spread of numbers/randomness
    const selectedItems: Record<string, boolean> = {};
    for (let i = 0; i < 100; i++) {
      const item = random2DItem(list2D);
      selectedItems[item] = true;
    }

    const firstItem = list2D[0][0];
    const lastItem =
      list2D[list2D.length - 1][list2D[list2D.length - 1].length - 1];

    expect(selectedItems[firstItem]).toBe(true);
    expect(selectedItems[lastItem]).toBe(true);
  });

  // NOTE: Disabled functionality as this is unlikely
  /*it("Handles empty list", () => {
    const emptyList: number[][] = [];
    const emptyListNested: number[][] = [[]];

    const item = random2DItem(emptyList);
    const itemNested = random2DItem(emptyListNested);

    expect(item).toBe(null);
    expect(itemNested).toBe(null);
  });*/
});
