// Utilities
import { deepEqual, isObject } from "@utilities/object";

describe("Checks whether an item is an object", () => {
  it("Detects objects", () => {
    const object = { something: "true" };

    expect(isObject(object)).toBe(true);
  });

  it("Detects non-objects", () => {
    const list: number[] = [];
    const bool = true;

    expect(isObject(list)).toBe(false);
    expect(isObject(bool)).toBe(false);
  });
});

describe("Checks whether objects are deeply equal", () => {
  it("Detects non-equality of dissimilar non-nested objects", () => {
    const object1 = { something: "else", check: true };
    const object2 = { something: "else", index: 0 };
    const object3 = { index: 0 };

    expect(deepEqual(object1, object2)).toBe(false);
    expect(deepEqual(object1, object3)).toBe(false);
  });

  it("Detects equality of similar non-nested objects", () => {
    const object1 = { something: "else", check: true, index: 0 };
    const object2 = { check: true, something: "else", index: 0 };

    expect(deepEqual(object1, object2)).toBe(true);
  });

  it("Detects non-equality of similar non-nested objects", () => {
    const object1 = { something: "else", check: true, index: 0 };
    const object2 = { check: false, something: "else", index: 0 };

    expect(deepEqual(object1, object2)).toBe(false);
  });

  it("Detects equality of similar nested objects", () => {
    const object1 = { nested: { check: true, index: 0 }, something: "else" };
    const object2 = { something: "else", nested: { index: 0, check: true } };

    expect(deepEqual(object1, object2)).toBe(true);
  });

  it("Detects non-equality of similar nested objects", () => {
    const object1 = { something: "else", nested: { check: true, index: 0 } };
    const object2 = { something: "else", nested: { check: false, index: 0 } };

    expect(deepEqual(object1, object2)).toBe(false);
  });
});
