/**
 * Check whether two objects are deeply equal
 *
 * Taken from: https://dmitripavlutin.com/how-to-compare-objects-in-javascript/#4-deep-equality
 *
 * @param   object1 - First object
 * @param   object2 - Second object
 * @returns Whether objects are deeply equal
 */
const deepEqual = (
  object1: Record<any, any>,
  object2: Record<any, any>,
): boolean => {
  const keys1 = Object.keys(object1);
  const keys2 = Object.keys(object2);

  if (keys1.length !== keys2.length) return false;

  for (const key of keys1) {
    const val1 = object1[key];
    const val2 = object2[key];
    const areObjects = isObject(val1) && isObject(val2);
    if (
      (areObjects && !deepEqual(val1, val2)) ||
      (!areObjects && val1 !== val2)
    ) {
      return false;
    }
  }

  return true;
};

/**
 * Check whether an item is an object
 *
 * @param   object - Item to check
 * @returns Whether item is an object
 */
const isObject = (object: any): boolean => {
  return object != null && typeof object === "object" && !Array.isArray(object);
};

export { deepEqual, isObject };

/* eslint @typescript-eslint/no-use-before-define: off, @typescript-eslint/no-explicit-any: off */
