// Types
import { Coordinate } from "./app";
import { MinesweeperDifficulty, MinesweeperTileState } from "./enums";

/** Minesweeper config */
export interface MinesweeperConfig {
  /** Board size options */
  boardSizeOptions: number[];
  /** Board tile size (pixels) */
  tileSize: number;
}

/** Minesweeper per-game config */
export interface MinesweeperGameConfig {
  /** Game difficulty */
  difficulty: MinesweeperDifficulty;
  /** Board width */
  boardWidth: number;
  /** Board height */
  boardHeight: number;
}

/** Minesweeper board tile */
export interface MinesweeperTile {
  /** Number of neighbouring bomb tiles */
  bombCount: number;
  /** Tile coordinates */
  coordinates: Coordinate;
  /** Whether tile has a bomb */
  bomb: boolean;
  /** Minesweeper tile state */
  state: MinesweeperTileState;
}
