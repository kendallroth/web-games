import { Coordinate } from "./app";

/** FloodIt config/settings object */
interface FloodItConfig {
  /** Board tile colors */
  colors: string[];
  /** Available tile color sets */
  boardColorOptions: number[];
  /** Board sizes (square) */
  boardSizeOptions: number[];
  /** Board tile size */
  tileSize: number;
}

/** FloodIt per-game configuration */
export interface FloodItGameConfig {
  /** Number of colours on board */
  boardColors: number;
  /** Board size (square) */
  boardSize: number;
}

/** FloodIt board tile */
export interface FloodItTile {
  /** Tile color */
  color: string;
  /** Tile coordinates */
  coordinates: Coordinate;
}
