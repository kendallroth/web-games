/** 2D Coordinate structure */
export interface Coordinate {
  /** Coordinate x position */
  x: number;
  /** Coordinate y position */
  y: number;
}

/** Game definition */
export interface Game {
  /** WARNING: Dynamically required component for router ONLY! */
  _component: Promise;

  /** Game icon */
  icon: string | null;
  /** Game image */
  image: string;
  /** Game name */
  name: string;
  /** Game route */
  route: string;
}

/** Simple global data store */
export interface Store {
  game: Game | null;
}
