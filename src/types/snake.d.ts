// Types
import { Coordinate } from "./app";
import { SnakeDirection, SnakeTileState } from "./enums";

/** Snake config */
export interface SnakeConfig {
  /** Board size options */
  boardSizeOptions: number[];
  /** Snake movement speed (seconds) */
  movementSpeed: number;
  /** Starting snake size */
  startingSize: number;
  /** Starting snake movement direction */
  startingDirection: SnakeDirection;
  /** Board tile size (pixels) */
  tileSize: number;
}

/** Snake per-game config */
export interface SnakeGameConfig {
  /** Board width */
  boardWidth: number;
  /** Board height */
  boardHeight: number;
}

/** Snake board tile */
export interface SnakeTile {
  /** Tile coordinates */
  coordinates: Coordinate;
  /** Snake tile state */
  state: SnakeTileState;
}
