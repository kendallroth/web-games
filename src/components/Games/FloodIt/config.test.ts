// Utilities
import {
  defaultGameConfig,
  localStorageKey,
  loadFloodItConfig,
  saveFloodItConfig,
} from "./config";

// Types
import { FloodItGameConfig } from "@typings/flood-it";

describe("Saves and loads flood it config", () => {
  beforeEach(() => {
    localStorage.clear();
    // @ts-ignore
    localStorage.getItem.mockClear();
    // @ts-ignore
    localStorage.setItem.mockClear();
  });

  it("Saves a flood it config", () => {
    const config: FloodItGameConfig = {
      boardColors: 4,
      boardSize: 20,
    };
    const configString = JSON.stringify(config);

    saveFloodItConfig(config);

    expect(localStorage.setItem).toHaveBeenCalledWith(
      localStorageKey,
      configString,
    );
    expect(localStorage.__STORE__[localStorageKey]).toBe(configString);
  });

  it("Loads a flood it config", () => {
    const savedConfig: FloodItGameConfig = {
      boardColors: 4,
      boardSize: 20,
    };

    saveFloodItConfig(savedConfig);
    const config = loadFloodItConfig();

    expect(localStorage.getItem).toHaveBeenCalledWith(localStorageKey);
    expect(config).toEqual(savedConfig);
  });

  it("Loads the default flood it config if not found", () => {
    const config = loadFloodItConfig();

    expect(localStorage.getItem).toHaveBeenCalledWith(localStorageKey);
    expect(config).toEqual(defaultGameConfig);
  });

  it("Loads and overrides an incorrect flood it config", () => {
    const savedConfig: FloodItGameConfig = {
      boardColors: 20,
      boardSize: 1,
    };

    saveFloodItConfig(savedConfig as FloodItGameConfig);
    const config = loadFloodItConfig();

    expect(localStorage.getItem).toHaveBeenCalledWith(localStorageKey);
    expect(config).toEqual(defaultGameConfig);
  });
});
