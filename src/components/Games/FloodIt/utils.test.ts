// Utilities
import floodItConfig from "./config";
import { calculateMaxMoves, generateTiles } from "./utils";

// Types
import { Coordinate } from "@typings/app";

describe("Calculates maximum number of moves", () => {
  it("Calculates simple number of moves", () => {
    const boardSize = 10;
    const colors = 3;

    const maxMoves = calculateMaxMoves(boardSize, colors);

    expect(maxMoves).toBe(10);
  });

  it("Calculates complex number of moves", () => {
    const boardSize = 24;
    const colors = 8;

    const maxMoves = calculateMaxMoves(boardSize, colors);

    expect(maxMoves).toBe(68);
  });
});

describe("Generates board tiles", () => {
  it("Generates board with correct sizing", () => {
    const size = 5;
    const tiles = generateTiles(size, floodItConfig.colors);

    const xLength = tiles.length;
    const yLength = tiles[0].length;
    const tileCount = tiles.reduce((rowAccum, row) => rowAccum + row.length, 0);

    expect(xLength).toBe(size);
    expect(yLength).toBe(size);
    expect(tileCount).toBe(size * size);
  });

  it("Generates board with correct coordinates", () => {
    const tiles = generateTiles(5, floodItConfig.colors);

    const wrongCoordinates: Coordinate[] = [];
    for (let y = 0; y < tiles.length; y++) {
      for (let x = 0; x < tiles[y].length; x++) {
        const coordinates = tiles[y][x].coordinates;
        if (x !== coordinates.x || y !== coordinates.y) {
          wrongCoordinates.push(coordinates);
        }
      }
    }

    expect(wrongCoordinates).toStrictEqual([]);
  });
});
