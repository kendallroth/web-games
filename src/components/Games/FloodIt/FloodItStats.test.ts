import { shallowMount, Wrapper } from "@vue/test-utils";
import colors from "vuetify/lib/util/colors";

// Components
import FloodItStats from "./FloodItStats.vue";

describe("FloodItStats component", () => {
  const getProps = (turns: number, maxTurns: number) => ({
    propsData: { maxTurns, turns },
  });

  const getWrapper = (
    turns: number,
    maxTurns: number,
  ): Wrapper<FloodItStats & { [key: string]: any }> => {
    return shallowMount(FloodItStats, getProps(turns, maxTurns));
  };

  describe("Calculates stats text color", () => {
    it("Calculates normal (green) stats color", () => {
      const wrapper = getWrapper(0, 10);

      expect(wrapper.vm.turnColor).toBe(colors.teal.base);
    });

    it("Calculates warning (amber) stats color", () => {
      const wrapper = getWrapper(7, 10);

      expect(wrapper.vm.turnColor).toBe(colors.amber.base);
    });

    it("Calculates warning (orange) stats color", () => {
      const wrapper = getWrapper(8, 10);

      expect(wrapper.vm.turnColor).toBe(colors.orange.base);
    });

    it("Calculates danger (red) stats color", () => {
      const wrapper = getWrapper(9, 10);

      expect(wrapper.vm.turnColor).toBe(colors.red.base);
    });
  });
});
