// Utilities
import {
  defaultGameConfig,
  loadSnakeConfig,
  saveSnakeConfig,
  localStorageKey,
} from "./config";

// Types
import { SnakeGameConfig } from "@typings/snake";

describe("Saves and loads snake config", () => {
  beforeEach(() => {
    localStorage.clear();
    // @ts-ignore
    localStorage.getItem.mockClear();
    // @ts-ignore
    localStorage.setItem.mockClear();
  });

  it("Saves a snake config", () => {
    const config: SnakeGameConfig = {
      boardHeight: 10,
      boardWidth: 20,
    };
    const configString = JSON.stringify(config);

    saveSnakeConfig(config);

    expect(localStorage.setItem).toHaveBeenCalledWith(
      localStorageKey,
      configString,
    );
    expect(localStorage.__STORE__[localStorageKey]).toBe(configString);
  });

  it("Loads a snake config", () => {
    const savedConfig: SnakeGameConfig = {
      boardHeight: 10,
      boardWidth: 20,
    };

    saveSnakeConfig(savedConfig);
    const config = loadSnakeConfig();

    expect(localStorage.getItem).toHaveBeenCalledWith(localStorageKey);
    expect(config).toEqual(savedConfig);
  });

  it("Loads the default snake config if not found", () => {
    const config = loadSnakeConfig();

    expect(localStorage.getItem).toHaveBeenCalledWith(localStorageKey);
    expect(config).toEqual(defaultGameConfig);
  });

  it("Loads and overrides an incorrect snake config", () => {
    // NOTE: Cannot use actual type since we are faking the difficulty
    const savedConfig: SnakeGameConfig = {
      boardHeight: 1,
      boardWidth: 1,
    };

    saveSnakeConfig(savedConfig);
    const config = loadSnakeConfig();

    expect(localStorage.getItem).toHaveBeenCalledWith(localStorageKey);
    expect(config).toEqual(defaultGameConfig);
  });
});
