// Utilities
import { deepEqual } from "@utilities/object";

// Types
import { SnakeTileState } from "@typings/enums";
import { SnakeGameConfig, SnakeTile } from "@typings/snake";

import { generateTiles } from "./utils";

const testConfig: SnakeGameConfig = {
  boardHeight: 15,
  boardWidth: 15,
};

describe("Generates snake board", () => {
  it("Generates board with correct sizing", () => {
    const config = { ...testConfig };
    const tiles = generateTiles(config);

    const xLength = tiles.length;
    const yLength = tiles[0].length;
    const tileCount = tiles.reduce((rowAccum, row) => rowAccum + row.length, 0);

    expect(xLength).toBe(config.boardWidth);
    expect(yLength).toBe(config.boardHeight);
    expect(tileCount).toBe(config.boardWidth * config.boardHeight);
  });

  it("Generates board with correct coordinates", () => {
    const config = { ...testConfig };
    const tiles = generateTiles(config);

    let areCoordinatesCorrect = true;
    for (let y = 0; y < tiles.length; y++) {
      for (let x = 0; x < tiles[y].length; x++) {
        const coordinates = tiles[y][x].coordinates;
        if (x !== coordinates.x || y !== coordinates.y) {
          areCoordinatesCorrect = false;
          break;
        }
      }
    }

    expect(areCoordinatesCorrect).toStrictEqual(true);
  });

  it("Generates board with proper defaults", () => {
    const config = { ...testConfig };
    const tiles = generateTiles(config);
    const defaultTile: SnakeTile = {
      coordinates: { x: 0, y: 0 },
      state: SnakeTileState.OPEN,
    };

    let areTilesCorrect = true;
    for (let y = 0; y < tiles.length; y++) {
      for (let x = 0; x < tiles[y].length; x++) {
        const isCorrect = deepEqual(tiles[y][x], {
          ...defaultTile,
          coordinates: { x, y },
        });

        if (!isCorrect) {
          areTilesCorrect = false;
          break;
        }
      }
    }

    expect(areTilesCorrect).toBe(true);
  });
});
