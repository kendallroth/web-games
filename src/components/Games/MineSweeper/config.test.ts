// Utilities
import {
  defaultGameConfig,
  getDifficultyFromIndex,
  getIndexFromDifficulty,
  localStorageKey,
  loadMinesweeperConfig,
  saveMinesweeperConfig,
} from "./config";

// Types
import { MinesweeperDifficulty } from "@typings/enums";
import { MinesweeperGameConfig } from "@typings/minesweeper";

describe("Converts between MinesweeperDifficulty and index", () => {
  beforeEach(() => {
    localStorage.clear();
    // @ts-ignore
    localStorage.getItem.mockClear();
    // @ts-ignore
    localStorage.setItem.mockClear();
  });

  it("Converts MinesweeperDifficulty to index", () => {
    const beginnerIndex = getIndexFromDifficulty(
      MinesweeperDifficulty.BEGINNER,
    );
    const intermediateIndex = getIndexFromDifficulty(
      MinesweeperDifficulty.INTERMEDIATE,
    );
    const expertIndex = getIndexFromDifficulty(MinesweeperDifficulty.EXPERT);

    expect(beginnerIndex).toBe(0);
    expect(intermediateIndex).toBe(1);
    expect(expertIndex).toBe(2);
  });

  it("Converts index to MinesweeperDifficulty", () => {
    const beginnerDifficulty = getDifficultyFromIndex(0);
    const intermediateDifficulty = getDifficultyFromIndex(1);
    const expertDifficulty = getDifficultyFromIndex(2);

    expect(beginnerDifficulty).toBe(MinesweeperDifficulty.BEGINNER);
    expect(intermediateDifficulty).toBe(MinesweeperDifficulty.INTERMEDIATE);
    expect(expertDifficulty).toBe(MinesweeperDifficulty.EXPERT);
  });
});

describe("Saves and loads minesweeper config", () => {
  beforeEach(() => {
    localStorage.clear();
    // @ts-ignore
    localStorage.getItem.mockClear();
    // @ts-ignore
    localStorage.setItem.mockClear();
  });

  it("Saves a minesweeper config", () => {
    const config: MinesweeperGameConfig = {
      difficulty: MinesweeperDifficulty.INTERMEDIATE,
      boardHeight: 10,
      boardWidth: 20,
    };
    const configString = JSON.stringify(config);

    saveMinesweeperConfig(config);

    expect(localStorage.setItem).toHaveBeenCalledWith(
      localStorageKey,
      configString,
    );
    expect(localStorage.__STORE__[localStorageKey]).toBe(configString);
  });

  it("Loads a minesweeper config", () => {
    const savedConfig: MinesweeperGameConfig = {
      difficulty: MinesweeperDifficulty.INTERMEDIATE,
      boardHeight: 10,
      boardWidth: 20,
    };

    saveMinesweeperConfig(savedConfig);
    const config = loadMinesweeperConfig();

    expect(localStorage.getItem).toHaveBeenCalledWith(localStorageKey);
    expect(config).toEqual(savedConfig);
  });

  it("Loads the default minesweeper config if not found", () => {
    const config = loadMinesweeperConfig();

    expect(localStorage.getItem).toHaveBeenCalledWith(localStorageKey);
    expect(config).toEqual(defaultGameConfig);
  });

  it("Loads and overrides an incorrect minesweeper config", () => {
    // NOTE: Cannot use actual type since we are faking the difficulty
    const savedConfig: Record<string, any> = {
      difficulty: "INVALID",
      boardHeight: 1,
      boardWidth: 1,
    };

    saveMinesweeperConfig(savedConfig as MinesweeperGameConfig);
    const config = loadMinesweeperConfig();

    expect(localStorage.getItem).toHaveBeenCalledWith(localStorageKey);
    expect(config).toEqual(defaultGameConfig);
  });
});
