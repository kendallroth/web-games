// Utilities
import { deepEqual } from "@utilities/object";

// Types
import { MinesweeperDifficulty, MinesweeperTileState } from "@typings/enums";
import { MinesweeperGameConfig, MinesweeperTile } from "@typings/minesweeper";

import { calculateBombCount, generateTiles } from "./utils";

const testConfig: MinesweeperGameConfig = {
  difficulty: MinesweeperDifficulty.BEGINNER,
  boardHeight: 15,
  boardWidth: 15,
};

describe("Calculates bomb count", () => {
  const config = { ...testConfig };

  it("Calculates bomb count on easy", () => {
    config.difficulty = MinesweeperDifficulty.BEGINNER;

    const bombs = calculateBombCount(config);

    expect(bombs).toBe(24);
  });

  it("Calculates bomb count on intermediate", () => {
    config.difficulty = MinesweeperDifficulty.INTERMEDIATE;

    const bombs = calculateBombCount(config);

    expect(bombs).toBe(36);
  });

  it("Calculates bomb count on expert", () => {
    config.difficulty = MinesweeperDifficulty.EXPERT;

    const bombs = calculateBombCount(config);
    expect(bombs).toBe(45);
  });
});

describe("Generates minesweeper board", () => {
  it("Generates board with correct sizing", () => {
    const config = { ...testConfig };
    const tiles = generateTiles(config);

    const xLength = tiles.length;
    const yLength = tiles[0].length;
    const tileCount = tiles.reduce((rowAccum, row) => rowAccum + row.length, 0);

    expect(xLength).toBe(config.boardWidth);
    expect(yLength).toBe(config.boardHeight);
    expect(tileCount).toBe(config.boardWidth * config.boardHeight);
  });

  it("Generates board with correct coordinates", () => {
    const config = { ...testConfig };
    const tiles = generateTiles(config);

    let areCoordinatesCorrect = true;
    for (let y = 0; y < tiles.length; y++) {
      for (let x = 0; x < tiles[y].length; x++) {
        const coordinates = tiles[y][x].coordinates;
        if (x !== coordinates.x || y !== coordinates.y) {
          areCoordinatesCorrect = false;
          break;
        }
      }
    }

    expect(areCoordinatesCorrect).toStrictEqual(true);
  });

  it("Generates board with proper defaults", () => {
    const config = { ...testConfig };
    const tiles = generateTiles(config);
    const defaultTile: MinesweeperTile = {
      bombCount: 0,
      coordinates: { x: 0, y: 0 },
      bomb: false,
      state: MinesweeperTileState.HIDDEN,
    };

    let areTilesCorrect = true;
    for (let y = 0; y < tiles.length; y++) {
      for (let x = 0; x < tiles[y].length; x++) {
        const isCorrect = deepEqual(tiles[y][x], {
          ...defaultTile,
          coordinates: { x, y },
        });

        if (!isCorrect) {
          areTilesCorrect = false;
          break;
        }
      }
    }

    expect(areTilesCorrect).toBe(true);
  });
});
