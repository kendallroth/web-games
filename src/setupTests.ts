/**
 * Global setup for Jest tests
 */

import "jest-localstorage-mock";
import Vue from "vue";
import Vuetify from "vuetify";

// Register Vuetify components (to avoid tests complaining about unregistered components)
Vue.use(Vuetify);
